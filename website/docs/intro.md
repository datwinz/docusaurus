---
sidebar_position: 1
title: HPE Proliant
hide_title: true
slug: /
---

# HPE ProLiant ML310e Gen8 v2 Server

Ik probeer op een oude server een labomgeving op te zetten. In deze docs hou ik mijn notities bij over dat proces. Op de server stond eerst windows 7, maar dan wel afgesloten met wachtwoorden die niet bekend waren. Dus ik begon met linux erop zetten en firmware updaten. Dat eerste deel staat hieronder beschreven. Helemaal onderaan staan ook de geinstalleerde packages en de firewall-instellingen voor de server en de vms.

## Fimware updaten

[CentOS 6](https://vault.centos.org/6.10/isos/x86_64/) of [7](http://mirror.widexs.nl/ftp/pub/os/Linux/distr/centos/7.9.2009/isos/x86_64/) installeren, 6 kan waarschijnlijk geen verbinding maken met websites vanwege veranderingen in SSL/TLS.

Links voor firmware en handleidingen:

* [HPE Drivers and Software](https://support.hpe.com/connect/s/product?language=en_US&tab=driversAndSoftware&kmpmoid=5379533&driversAndSoftwareFilter=8000012)
* [HPE ILO 4 Drivers and Software](https://support.hpe.com/connect/s/product?language=en_US&kmpmoid=1009143853&tab=driversAndSoftware&driversAndSoftwareFilter=8000113)
* [Intelligent Provisioning Recovery Media](https://support.hpe.com/connect/s/softwaredetails?language=en_US&softwareId=MTX_bf5521c396f2440bb0cb5efa87)
* [HPE Manuals and Guides](https://support.hpe.com/connect/s/product?language=en_US&tab=manualsAndGuides&kmpmoid=5379533&driversAndSoftwareFilter=8000012)
* [Kernel panic advisory (doet niks meer)](https://support.hpe.com/hpesc/public/docDisplay?docId=emr_na-c03732112)
* [Werkgeheugen configureren](https://support.hpe.com/hpesc/public/docDisplay?docLocale=en_US&docId=c03912646)
* [Intel ark processorinformatie](https://ark.intel.com/content/www/us/en/ark/products/75052/intel-xeon-processor-e31220-v3-8m-cache-3-10-ghz.html)

## OS installeren

De ProLiant gebruikt de Smart Array B120i RAID card om te booten. Dus je moet aan een aantal dingen denken bij het installeren van een nieuw OS:

- Stel eerst de RAID card in op de level die je wil. Dat doe je in Intelligent Provisioning (F10 bij opstarten) en dan het juiste menu kiezen. Als je het level en array hebt ingesteld, moet je ook checken of de RAID card de boot controller is (oftewel of de ProLiant vanaf de ingestelde kaart opstart). Dat doe je in hetzelfde menu.
- De harde schijven moeten een bestandssysteem hebben. Ook de schijf waar je je OS niet installeert. ext4 werkt waarschijnlijk bij Linux, exFAT bij windows.
- Je moet bij het installeren van de OS niet ook software RAID instellen. Dan vechten ze tegen mekaar en start de ProLiant niet op. Je moet als je hardware RAID 1 doet ook maar op 1 schijf je OS installeren en de andere schijf dus leeglaten.

Intelligent Provisioning Recovery Media moet op een cd/dvd staan. Misschien moet je ook ILO resetten voordat je het erop kan zetten.

Hardware RAID werkt niet fijn met Linux/nieuwe versies van Linux/nieuwe firmware (een van de drie). Dus hardware RAID staat aan op schijf 1 (/dev/sda/) met RAID 0, software RAID staat aan van Rocky op beide schijven met RAID 1.

Installeren Rocky 8.7 als virtualization server met alles aan behalve de graphical dingen. Een extra partitie maken voor /var/lib/libvirt. De standaardlocatie is namelijk onder de partitie gemount op/en je wil waarschijnlijk meer ruimte dan dat. Als je installeert eerst een user-account maken met adminstrator-rechten, dan hoef je geen root-wachtwoord aan te maken. Als je een aparte partitie hebt gemaakt voor libvirt, moet je de selinux context restoren in de /var/lib/libvirt map. Anders gaat het zeuren dat het de bestanden voor het virtuele netwerk niet kan lezen. Dat doe je met ```sudo restorecon -r /var/lib/libvirt```.

## Packages

### Host OS

* langpacks-nl
* make
* docker-ce
* cockpit-machines
* python3-libvirt

#### remove

* podman
* runc
* podman-docker
* podman-compose

#### Repos

* ```sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo```

### server

* langpacks-nl
* ```module idm:DL1/{dns,adtrust}```

#### remove

* cockpit
* cockpit-ws
* libvirt
* qemu-kvm

### workstation

* langpacks-nl
* ```module idm:DL1/client```

#### remove

* cockpit
* cockpit-ws

## Firewall

Standaardservices: cockpit dhcpv6-client ssh

### Host OS

### ca

```
sudo firewall-cmd --permanent --add-service=https
```

### server

```
sudo firewall-cmd --permanent --add-service={freeipa-4,dns,ntp,https,http}
```

### workstation
