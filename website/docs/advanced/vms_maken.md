---
sidebar_position: 1
title: VM's maken
hide_title: true
---

## De vms maken

* [community.libvirt.virt](https://docs.ansible.com/ansible/latest/collections/community/libvirt/virt_module.html#ansible-collections-community-libvirt-virt-module)

Aan de hand van dit [lab build artikel](https://www.redhat.com/sysadmin/build-VM-fast-ansible) de vms maken met de [rocky cloud images](https://dl.rockylinux.org/pub/rocky/8/images/x86_64/). Het downloaden van de image duurt lang, je ziet geen vooruitgangsbalk en het bestand waar het naar gedownload wordt, wordt ook niet groter. Hierdoor heb je geen makkelijke indicatie hoe lang dat duurt. Dus dat is een kwestie van geduld hebben.

Ik probeer bridge mode in te stellen voor de vms, zodat de vms in hetzelfde netwerk draaien als de host. Hulp staat in deze [twee](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_virtualization/configuring-virtual-machine-network-connections_configuring-and-managing-virtualization#configuring-externally-visible-virtual-machines-using-the-command-line-interface_recommended-virtual-machine-networking-configurations-using-the-command-line-interface) [Red Hat](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_networking/configuring-a-network-bridge_configuring-and-managing-networking) artikels. Van Red Hat moet je eerst een network bridge device (met ```ip``` en ```nmcli```) maken, daarvoor heb je of twee echte of virtuele netwerkpoorten nodig. Aangezien ik maar 1 poort heb aangesloten moet ik een virtuele maken. Ik weet alleen niet hoe, dus zocht ik door naar een andere oplossing. Daardoor vond ik [dit Red Hat artikel](https://developers.redhat.com/blog/2018/10/22/introduction-to-linux-interfaces-for-virtual-networking?source=sso#ipvlan) met alle mogelijke virtuele interfaces in linux. Door de info in het artikel over MACVTAP devices kwam op [dit IBM artikel](https://www.ibm.com/docs/en/linux-on-systems?topic=choices-using-macvtap-driver) uit. De configuratie in het IBM artikel werkt, dus die ga ik gebruiken.

Het is handig als de vms statische IP-addressen hebben, je kan die alleen niet instellen in de template die je gemaakt hebt aan de hand van het lab build artikel. Je kan wel MAC-addressen instellen, dus dat doen en dan met je router de IP-addressen statisch maken. Dit maakt het wel minder "portable", maar dat is dan maar zo.

Het lijkt erop dat het bugt als je een andere gebruiker gebruikt dan je bent, terwijl je de playbooks uitvoert en ansible-vault gebruikt. Ik heb namelijk een ansible gebruiker gemaakt, als die laat ik de playbooks uitvoeren door het zo in te stellen in het hosts(.yml) bestand. In het hosts bestand heb ik geprobeert de variabelen te encrypten met ansible-vault, [deze tip voor variabelen](https://docs.ansible.com/ansible/latest/tips_tricks/ansible_tips_tricks.html#tip-for-variables-and-vaults) geprobeert en geprobeert het hele hosts-bestand te encrypten. Bij alledrie loopt het vast bij "Gathering facts", specifiek bij het ```/bin/sh -c 'sudo -H -S -p "[sudo via ansible,*``` command, met de melding "Sorry, probeer het opnieuw"/"Sorry, try again". Deze melding komt van ```sudo``` af. Als je de gebruiker gebruikt waarmee je momenteel bent ingelogd, lijkt ansible-playbook deze melding niet te geven.
Misschien moet de ansible gebruiker in de libvirt groep zitten of moet het een home map hebben. Daarnaast heb ik ansible_user in het hosts bestand en remote_user in de playbook gedefinieert, misschien raakt het daardoor in de war. Testen lijkt te laten zien dat de gebruiker een home map moet hebben.

Om de juiste ansible gebruiker te maken doe je:

```
sudo useradd --system --create-home --home-dir /home/ansible --skel /etc/skel ansible
sudo passwd ansible
```
