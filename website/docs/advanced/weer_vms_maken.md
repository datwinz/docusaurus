---
sidebar_position: 2
title: Opnieuw VM's maken
hide_title: true
---

## Opnieuw VM's maken

De standaard cloud images van Rocky Linux hebben een harde schijf van 10GB. Dat is niet groot genoeg. Om dat te fixen ga ik handmatig 3 nieuwe vms maken met de minimale installatie van Rocky Linux. ca geef ik 25GB, server 400GB en workstation 100GB. Ik maak die met alleen een root account en een makkelijk wachtwoord. Ik sla ze op als small, medium en large images in de git dir samen met de andere playbooks. Ik zou [git-lfs](https://git-lfs.com/) kunnen gebruiken om ze eraan toe te voegen. Git-lfs is toch geen goede optie, het heeft geen ondersteuning voor thing provision qcow2 images en doet het tegenovergestelde van wat ik wil. Ik wil dat het de bestanden met pointers op de git server opslaat en update en dat de images zelf op git client blijven staan. Het doet dus juist het tegenovergestelde.

Ik moet nu wel de kvm_provision role aanpassen, zodat je kan kiezen welke image je wil, zodat het geen image meer download en zodat het weet waar de images staan. Deze bestanden zijn wel groter dan de cloud image. qcow2 images worden vanzelf groter naarmate er meer data in komt te staan. Linux denkt (als je ```ls``` of ```df``` doet) alleen wel dat het de maximale grote is, ```du``` doet het wel goed. Hierom duurt het kopieren naar de /var/lib/libvirt/images dir een stuk langer. Als je echter eerst ```qemu-img convert image.qcow2 -O qcow2 -c image_compressed.qcow2``` doet, worden de images veel kleiner en klopt ```ls``` ook. De functie van de images blijven dan hetzelfde. De shasums van de images zijn:

```
3c56d440ff34b63296633076c90b57244f892ac6463dafdde866adc2a37d66ff  small_UNSAFE_PASSWORD.qcow2
ef880765f58bdaaadc2bde5743f19596490ff7549ac144d18596f3407be38752  medium_UNSAFE_PASSWORD.qcow2
1cfeac6ca57e1537c290f2c5cd60718168083a97b765bd252679d5972796b864  large_UNSAFE_PASSWORD.qcow2
```

Nadat de bestanden gekopieerd zijn, kan het zijn dat ze de volledige grootte in ruimte opnemen. Zie [dit artikel](https://linuxconfig.org/how-to-resize-a-qcow2-disk-image-on-linux) om dat te verhelpen. Misschien moet je ook dd if=/dev/zero of=dummyfile doen zoals [hier beschreven](https://stackoverflow.com/a/37337628).
