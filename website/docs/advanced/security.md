---
sidebar_position: 5
title: Security implementaties
hide_title: true
---

## SIEM en SCA

[Wazuh](https://wazuh.com/) als SIEM. Installeren met onderstaand commando op de server. Dit is dus niet in een Docker container. Er zit daar misschien al SCA in ingebouwd, maar zo niet kan ik nog [trivy](https://trivy.dev/) toevoegen. Misschien voegt [Nessus](https://www.tenable.com/) ook wat toe.

```
curl -sO https://packages.wazuh.com/4.7/wazuh-install.sh && sudo bash ./wazuh-install.sh -a
```

## Code security tests

Security tests kunnen waarschijnlijk in gitea gezet worden. SAST kan met [semgrep](https://semgrep.dev/) en DAST met [nuclei](https://github.com/projectdiscovery/nuclei) en [OWASP Zap](https://www.zaproxy.org/docs/docker/about/).

## Services achter HTTPS

Met [nginx proxy manager](https://nginxproxymanager.com/) kun je SSL/TLS certificaten instellen. Dan moeten de services wel online (oftewel op het internet) staan, maar dat staat het bij mij niet. Je kan ook een DNS challenge doen. Daarvoor heeft nginx proxy manager een lijst van DNS-servers, en de mijne staat er niet tussen. Ook in de [uitgebreidere lijst](https://community.letsencrypt.org/t/dns-providers-who-easily-integrate-with-lets-encrypt-dns-validation/86438) staat die van mij niet. Het kan misschien wel met een van de "Self-Hosted" opties onderaan de pagina, bijvoorbeeld [acme-dns](https://github.com/joohoi/acme-dns).

Met [caddy](https://caddyserver.com/docs/getting-started) kun je meestal ook automatisch SSL/TLS certificaten instellen. Ik heb het geprobeert in te stellen met .local domeinnamen en named aangepast zodat de DNS naar die domeinen wijst. Maar het is niet zo stabiel.

Dus dan maar met EJBCA TLS-certificaten maken, op [deze pagina](https://doc.primekey.com/ejbca/solution-areas/issuing-tls-certificates) en [deze pagina](https://doc.primekey.com/ejbca/tutorials-and-guides/tutorial-issue-tls-client-certificates-with-ejbca) staat oude info. Er zijn een aantal [nieuwe instructies](https://doc.primekey.com/ejbca/ejbca-operations/ejbca-operations-guide/ca-operations-guide/managing-cas#ManagingCAs-CreatingCAs) online gekomen, waar ook meer te scripten is.

Als je infrastructuur staat, moet je TLS certificaten maken zoals [in deze tutorial](https://docs.keyfactor.com/ejbca/latest/tutorial-issue-tls-server-certificates-with-ejbca). Zoals je ziet, moet je eerst een key genereren met ```openssl```. Of op [deze manier](https://docs.keyfactor.com/ejbca/latest/tutorial-issue-tls-client-certificates-with-ejbca) en dan [de certificaten en de key](https://ehealth-aussie.blogspot.com/2013/06/convert-p12-bundle-to-server.html) uit het p12-bestand halen met ```openssl```.
