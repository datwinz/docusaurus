---
sidebar_position: 3
title: Packages en firewall
hide_title: true
---

## De packages installeren en firewall instellen

Op vm1/server zijn de volgende "dnf groups" geinstalleerd: ```Server Container Management .NET Core Development RPM Development Tools Ontwikkelingshulpmiddelen Headless Management Legacy UNIX Compatibility Netwerkservers Scientific Support Security Tools Smart Card Support Systeemgereedschappen```. Op vm2/workstation zijn de volgende "dnf groups" geinstalleerd: ```Workstation Container Management .NET Core Development RPM Development Tools Ontwikkelingshulpmiddelen Graphical Administration Tools Headless Management Legacy UNIX Compatibility Netwerkservers Scientific Support Security Tools Smart Card Support Systeemgereedschappen```.

Volgens mij heb ik niet al deze dnf groups nodig. Helemaal omdat ik ook weer een aantal packages verwijder om docker te installeren. Voor nu is dit prima, maar later maar even naar kijken of ik wat kan verwijderen.

Omdat de vms zijn gemaakt met MACVTAP devices, oftewel dat ze in hetzelfde netwerk als de host OS zitten, kun je niet direct vanaf de host ssh-en of ping-en naar de vms. Hij kan geen route vinden naar de vms. [Dit artikel](https://web.archive.org/web/20240518192935/https://www.furorteutonicus.eu/2013-08-04-enabling-host-guest-networking-with-kvm-macvlan-and-macvtap) heeft een oplossing: ook een MACVTAP device maken op de host vm. SELinux blokkeert de systemd unit, maar ```journalctl -xe``` geeft commando's om dat te fixen. Het moet een module generen met ```ausearch``` en ```audit2allow``` en dat aanzetten met ```semodule```, daarnaast moeten de SELinux context goed staan op de ```/usr/local/bin/``` map. Dit maakt de verbindingen naar de host OS wel onstabiel. Soms verliest het verbinding en duurt het een minuut voordat het weer verbinding maakt. Dit komt waarschijnlijk om dat er veel in en uit verbindingen door dezelfde fysieke poort gaan en ze dan botsen. Ik zie dit alleen niet in de standaard logs terug.

De package ```NetworkManager-config-server``` breekt het netwerk op de vm. Het heeft nog wel een mac-adres maar kan geen verbinding meer maken met het netwerk. Ook als je overgaat naar een andere modus, bijvoorbeeld NAT, of een andere device, blijft het kapot.

Ik moet nog een keer goed door ansible-vault heen gaan om te kijken hoe het werkt. Ik wil namelijk eerst inloggen als root en dan het account ansible maken met een geheim wachtwoord. Hiervoor moeten de variabelen voor root en voor ansible in twee verschillende hosts-bestanden o.i.d. staan. Daarnaast moet ik het geheime wachtwoord kunnen ophalen om het account te maken. Waar ik gebruik van kan maken zijn de group_vars en host_vars mappen, en het splitten in een vars en een vault bestand voor de geheimen. Hoe precies moet ik uitwerken. [Dit artikel over inventory](https://docs.ansible.com/ansible/latest/inventory_guide/intro_inventory.html#passing-multiple-inventory-sources) en [dit artikel over vars en vaults](https://docs.ansible.com/ansible/latest/tips_tricks/ansible_tips_tricks.html#keep-vaulted-variables-safely-visible) kunnen helpen. Verder staan in [dit artikel](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable) hoe de variabelen gelezen worden, van boven minst belangrijk naar beneden meest belangrijk.

Hieronder staat de mappenstructuur die ik bedacht heb. Je gebruikt bijna altijd de "main"-inventory. Alleen voor het maken van de "ansible" users moet je inloggen als root en gebruik je de "setup_users"-inventory. Daarna gebruik je weer de "main"-inventory en log je in als ansible. Om de "ansible"-gebruikers te maken, maak  je gebruik van de ansbile.builtin.user module. De wachtwoorden moet dan versleuteld worden met bijvoorbeeld dit commando: ```ansible all -i localhost, -m debug -a "msg={{ 'mypassword' | password_hash('sha512', '') }}"```. De ```''``` achter ```password_hash``` zorgt ervoor dat er een willekeurige salt wordt gebruikt.

```
ansible/
├── 1_init_setup/
│   ├── make_vms.yml
│   ├── roles/
│   │   ├── kvm-provision/
│   │   └── ansible-ssh-copy-id/
│   └── setup_vms.yml
├── ansible-playbook.sh
├── inventories/
│   ├── main
│   │   ├── hosts
│   │   └── host_vars/
│   │       ├── ca/
│   │       │   ├── vars
│   │       │   └── vault
│   │       ├── localhost/
│   │       │   ├── vars
│   │       │   └── vault
│   │       ├── server/
│   │       │   ├── vars
│   │       │   └── vault
│   │       └── workstation/
│   │           ├── vars
│   │           └── vault
│   └── setup_users/
│      ├── hosts
│      └── host_vars/
│          ├── ca/
│          │   ├── vars
│          │   └── vault
│          ├── localhost/
│          │   ├── vars
│          │   └── vault
│          ├── server/
│          │   ├── vars
│          │   └── vault
│          └── workstation/
│              ├── vars
│              └── vault
└── requirements.yml
```
