---
sidebar_position: 3
title: TODO
hide_title: true
---

## TODO

* ~~firmware updaten op serverkast, daarvoor centos 6 of 7 installeren en van HP Enterprise site firmware downloaden~~
* ~~Rocky Linux 8 installeren als headless virtualization server, voor libvirt aparte partitie maken gemount op /var/lib/libvirt, daarna met restorecon de SELinux context herstellen~~
* ~~kijken of vagrant werkt met libvirt, zo niet met virt-manager over ssh en met Spice als graphics protocol vm1/server en vm2/workstation installeren~~
* ~~DHCP vanuit virsh uitzetten en statisch IP op server instellen, checken of server nog verbinding maakt met internet, of je nog kan ssh-en naar server~~
* ~~ejbca-ce docker instellen op host OS als Root CA~~
* ~~ipa-server op server installeren als CA, LDAP-server, ejbca-ce op host OS als Root CA instellen~~
* ~~met boundary op server en workstation en host OS wachtwoorden en keys managen, moet volgens mij keys opslaan in ipa-server CA, checken of boundary's sshvariant werkt~~
* ~~met ansible op server configuratie op workstation instellen zodat bovenstaande dingen behouden blijven, op workstation software updates instellen~~
* ~~met ansible op host OS de vms beheren~~
* ~~met boundary host online gooien~~
* Met keycloak of zitadel dingen instellen
* docusaurus, taiga, wazuh

### Uiteindelijk

* VMs opnieuw maken met ansible, ejbca instellen met kubernetes, freeIPA opnieuw installeren met integrated bind dns
    * met kubernetes op server web server container en database server container maken, checken of containers vanaf workstation te bereiken zijn
* host OS, server en workstation hardenen: [Securing Red Hat Enterprise Linux 8](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/security_hardening/index)
* met let's encrypt ejbca, cockpit, freeipa en andere registreren. Kan waarschijnlijk niet, maar kan waarschijnlijk wel met [EJBCA TLS server](https://doc.primekey.com/ejbca/tutorials-and-guides/tutorial-issue-tls-server-certificates-with-ejbca) en [client certificates](https://doc.primekey.com/ejbca/tutorials-and-guides/tutorial-issue-tls-client-certificates-with-ejbca).
* devops omgeving hosten op server, met git, Scrum boards, CI/CD pipelines, DAST, SAST en SCA.
* met ansible op host OS alles in de TODO het bovenstaande beheren. Zijn packages voor met playbooks en roles: ```ansible-collection-redhat-rhel_mgmt``` en ```ansible-freeipa```.
