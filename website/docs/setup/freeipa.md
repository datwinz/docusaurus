---
sidebar_position: 3
title: FreeIPA
hide_title: true
---

## FreeIPA op server VM

Installeren aan de hand van [RHEL8 Installing Identity Management](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/installing_identity_management/preparing-the-system-for-ipa-server-installation_installing-identity-management) en [Red Hat Identity Management](https://web.archive.org/web/20230411210525/https://access.redhat.com/products/identity-management/), je moet misschien inloggen met een Red Hat developer account. Daarnaast heeft [FreeIPA on CentOS 7](https://www.digitalocean.com/community/tutorials/how-to-set-up-centralized-linux-authentication-with-freeipa-on-centos-7). [Linode Set Up a FreeIPA Server and Client](https://www.linode.com/docs/guides/freeipa-for-identity-management/) ook info.

IPv6 aanzetten door ```<network ipv6='yes'>``` te veranderen in het netwerk XML-bestand van libvirt. Daarnaast onder de IPv4-instellingen voor DHCP het onderstaande toevoegen met een geldig adres:

```
<ip family="ipv6" address="xxxx:xxxx:xxxx:xxxx::1" prefix="64">
  <dhcp>
    <range start="xxxx:xxxx:xxxx:xxxx::2" end="xxxx:xxxx:xxxx:xxxx::ff"/>
  </dhcp>
</ip>
```

Om FreeIPA te installeren moet je een domeinnaam registreren en bij je registrar je server toevoegen als A record in de DNS records. Dit doe je met de lokale IP's van je server, zowel IPv4 als IPv6, en met de hostname van je server, bijvoorbeeld server.mijndomein.nl. Je moet **niet** het IP-adres gebruiken waarmee de server aan het internet hangt (dus van het modem o.i.d.), dan faalt de installatie en zegt het dat het geen verbinding kan maken met ldap. Daarna moet je een aantal firewall ports openen: http of 80/tcp, https of 443/tcp, ldap of 389/tcp, ldaps of 636/tcp, kerberos of 88/tcp en 88/udp, kpasswd of 464/tcp en 464/udp, dns of 53/tcp en 53/udp, ntp of 123/udp. Dit kan allemaal apart of met ```sudo firewall-cmd --permanent --add-service={freeipa-4,dns,ntp}```, daarna ```sudo firewall-cmd --reload```. Daarna in /etc/hosts i.p.v. je loopbackadres, het lokale IP-adres invullen bij je hostname, bijvoorbeeld ```192.168.122.10 server.mijndomein.nl```. FreeIPA heeft een rng nodig, die zit ingebouwd in Linux maar die kan je ook apart downloaden, bijvoorbeeld rng-tools en dan de rngd service starten.

De idm module installeren met Active Directory support, dus ```sudo dnf module install idm:DL1/{dns,adtrust}```.

Om ervoor te zorgen dat het certificaat getekend kan worden door de EJBCA root CA, doe je ```sudo ipa-server-install --external-ca```. Je kan als ntp server de servers van [pool.ntp.org](https://www.pool.ntp.org/zone/nl) doen als pool-server. 

Belangrijke meldingen die je krijgt:

```
Certain directory server operations require an administrative user.
This user is referred to as the Directory Manager and has full access
to the Directory for system management tasks and will be added to the
instance of directory server created for IPA.

The IPA server requires an administrative user, named 'admin'.
This user is a regular system account used for IPA server administration.

The IPA Master Server will be configured with:
Hostname:       server.mijndomein.nl
IP address(es): <lokale IP-adressen>
Domain name:    mijndomein.nl
Realm name:     MIJNDOMEIN.NL

The CA will be configured with:
Subject DN:   CN=Certificate Authority,O=MIJNDOMEIN.NL
Subject base: O=MIJNDOMEIN.NL
Chaining:     externally signed (two-step installation)

NTP pool:	pool.ntp.org
```

Als de server geïnstalleerd is geeft het deze melding:

```
The next step is to get /root/ipa.csr signed by your CA and re-run /usr/sbin/ipa-server-install as:
/usr/sbin/ipa-server-install --external-cert-file=/path/to/signed_certificate --external-cert-file=/path/to/external_ca_certificate
The ipa-server-install command was successful
```

Dus moet je de het certificaat tekenen met EJBCA, zie waarschijnlijk [EJBCA Operations](https://doc.primekey.com/ejbca/ejbca-operations). Daar staat het niet in.

[User guide op mincit.gov.co](http://pki.mincit.gov.co:8080/ejbca/doc/userguide.html#Signing%20an%20External%20CA) geeft een aantal stappen om een "External CA" te signen. Dit helpt, de stappen zijn: het SUBCA certificate profile klonen en aanpassen, bijvoorbeeld validity naar 365d. Een end entity profile maken en daar O aan toevoegen bij de Subject DN Attributes. In De RA Web GUI onder enroll een New Request doen met het type van "JouwSUBCA", subtype "SUBCA", CA "JouwROOTCA" en Key-pair generation "Provided by user". Dan kun je de CSR van FreeIPA uploaden en wordt die gesignd. Je moet de volledige PEM-chain downloaden van het gesignde certificaat. Je moet ook de PEM downloaden van de root CA door ernaar te zoeken in Certificates in de RA Web GUI. Dan kun je bij view de PEM downloaden. De PEM's moet je gebruiken in het ```ipa-server-install``` command als ```/path/to/signed_certificate``` en ```/path/to/external_ca_certificate```. Dit moeten de volledige/absolute paden zijn. Als het commando klaar is zegt het dit:

```
Please add records in this file to your DNS system: /tmp/ipa.system.records.v7wko0h5.db
==============================================================================
Setup complete

Next steps:
	1. You must make sure these network ports are open:
		TCP Ports:
		  * 80, 443: HTTP/HTTPS
		  * 389, 636: LDAP/LDAPS
		  * 88, 464: kerberos
		UDP Ports:
		  * 88, 464: kerberos
		  * 123: ntp

	2. You can now obtain a kerberos ticket using the command: 'kinit admin'
	   This ticket will allow you to use the IPA tools (e.g., ipa user-add)
	   and the web user interface.

Be sure to back up the CA certificates stored in /root/cacert.p12
These files are required to create replicas. The password for these
files is the Directory Manager password
The ipa-server-install command was successful
```

Het cacert.p12-bestand opslaan op een veilige plek, bijvoorbeeld een versleutelde schijf(kopie).

Voor de DNS-records moet je [bind en bind-utils installeren](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/managing_networking_infrastructure_services/assembly_setting-up-and-configuring-a-bind-dns-server_networking-infrastructure-services) op de host OS. Je moet ```allow-query { localhost; 192.168.122.0/24; 2001:db8:1::/64; };``` en ```allow-recursion { localhost; 192.168.122.0/24; 2001:db8:1::/64; };``` toevoegen aan /etc/named.conf met de juiste IP-adressen. ~~Je moet ook ```forwarders { 198.168.1.1; };``` toevoegen aan /etc/named.conf met je oude DNS-server.~~ Hierna kun je het bestand checken met ```sudo named-checkconf```. Daarna moet je de dns service toevoegen aan firewall en de named service starten en enablen met systemd.

Hierna moet je een zone toevoegen voor de domein van je IPA, bijvoorbeeld mijndomein.nl. Voeg daarvoor het onderstaande toe:

```
zone "domeinnaam.nl" {
        type master;
        file "domeinnaam.nl.zone";
        allow-query { 192.168.122.0/24; 2001:db8:1::/64; };
        allow-transfer { none; };
};
```

Maak dan het bestand /var/named/domeinnaam.nl.zone. Daarin moeten de primaire, secundaire en eventueel tetriare nameserver van je registrar staan. Zie hieronder een voorbeeld:

```
$TTL 8h
@ IN SOA <primaire nameserver>. hostmaster.domeinnaam.nl. (
                          2022070601 ; serial number
                          1d         ; refresh period
                          3h         ; retry period
                          3d         ; expire time
                          3h )       ; minimum TTL

    IN NS	<primaire nameserver>.
    IN NS	<secundaire nameserver>.
    IN NS	<tertiaire nameserver>.

_kerberos-master._tcp.domeinnaam.nl. 3600 IN SRV 0 100 88 server.domeinnaam.nl.
_kerberos-master._udp.domeinnaam.nl. 3600 IN SRV 0 100 88 server.domeinnaam.nl.
_kerberos._tcp.domeinnaam.nl. 3600 IN SRV 0 100 88 server.domeinnaam.nl.
_kerberos._udp.domeinnaam.nl. 3600 IN SRV 0 100 88 server.domeinnaam.nl.
_kerberos.domeinnaam.nl. 3600 IN TXT "DOMEINNAAM.NL"
_kerberos.domeinnaam.nl. 3600 IN URI 0 100 "krb5srv:m:tcp:server.domeinnaam.nl."
_kerberos.domeinnaam.nl. 3600 IN URI 0 100 "krb5srv:m:udp:server.domeinnaam.nl."
_kpasswd._tcp.domeinnaam.nl. 3600 IN SRV 0 100 464 server.domeinnaam.nl.
_kpasswd._udp.domeinnaam.nl. 3600 IN SRV 0 100 464 server.domeinnaam.nl.
_kpasswd.domeinnaam.nl. 3600 IN URI 0 100 "krb5srv:m:tcp:server.domeinnaam.nl."
_kpasswd.domeinnaam.nl. 3600 IN URI 0 100 "krb5srv:m:udp:server.domeinnaam.nl."
_ldap._tcp.domeinnaam.nl. 3600 IN SRV 0 100 389 server.domeinnaam.nl.
ipa-ca.domeinnaam.nl. 3600 IN A 192.168.122.10
ipa-ca.domeinnaam.nl. 3600 IN AAAA f2001:db8:1::5e
```

Als dit gedaan is, zijn de DNS records wel goed op de host OS, maar niet op de server vm. Kan zijn dat de records nog geupdate moeten worden. Maar kan ook dat het op de server geinstalleerd moet worden of dat de [bridge instellingen](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/virtualization_deployment_and_administration_guide/sect-bridge-mode) anders moeten. De tweede is waarschijnlijker, het vraagt ook om "integrated DNS **(BIND)**" als je ```sudo ipa-server-install *``` uitvoert. Het is dus beter om FreeIPA opnieuw te installeren met bind/named ingesteld.

Daarna op de client, oftewel de workstation, FreeIPA installeren aan de hand van [Red Hat Installing an IdM Client](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/installing_identity_management/assembly_installing-an-idm-client_installing-identity-management). Daarna het opzetten met het ```ipa-client-install --enable-dns-updates --mkhomedir``` commando. Je moet hiervoor ook het A- en het AAAA-record toevoegen aan de DNS voor de client IP-adressen. Met [deze link](https://www.digitalocean.com/community/tutorials/how-to-configure-a-freeipa-client-on-centos-7#step-4-enabling-and-verifying-sudo-rules-optional) kun je ook sudo privileges toevoegen aan FreeIPA-gebruikers.

[Port forwarden zoals bij EJBCA](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/sec-port_forwarding) werkt niet, ik snap niet direct waarom. Met wat zoeken kwam ik erachter dat je hiervoor moet port-forwarden over zones heen, van "libvirt" naar "public", met behulp van [firewald policies](https://firewalld.org/documentation/man-pages/firewalld.policy.html). Je kan wel de webui gebruiken met een virtuele desktop m.b.v. virt-manager. Je kan misschien wel HTTP verzenden met Boundary.

[CERN](https://web.archive.org/web/20221114083223/https://auth.docs.cern.ch/freeipa/freeipa-ldap/) heeft een guide voor Free IPA, misschien handig.
