---
sidebar_position: 5
title: Kubernetes
hide_title: true
---

## Met K8s de docker containers voor EJBCA beheren

Kan het doen met ```kubeadm``` aan de hand van [installing kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/). Eerst een /etc/yum.repos.d/kubernetes.repo toevoegen met de tekst:

```
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
```

Dan installeren met ```sudo dnf install kubeadm --disableexcludes=kubernetes```. Hierna moet je k8s initialiseren, hiervoor moet je wel wat instellingen verandern. Ten eerste kube-apiserver als service en 10250/tcp toevoegen aan firewalld. Docker werkt niet meer standaard met k8s [sinds 2020](https://kubernetes.io/blog/2020/12/02/dont-panic-kubernetes-and-docker/), dus moet je [cri-docker](https://github.com/Mirantis/cri-dockerd) installeren aan de hand van de rpm-package. Ook moet je de systemd cri-docker.service en cri-docker.socket enablen, de socket moet je ook starten. Ook moet je dat k8s doorgeven dat je docker gebruikt en gaat het zeuren dat swap aan staat. Dat moet je [allebei](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/) [instellen](https://kubernetes.io/docs/reference/config-api/kubelet-config.v1beta1/#kubelet-config-k8s-io-v1beta1-KubeletConfiguration). Maak daarvoor een config.yaml-bestand met deze inhoud:

```
apiVersion: kubeadm.k8s.io/v1beta3
kind: InitConfiguration
nodeRegistration:
  criSocket: unix:///var/run/cri-dockerd.sock
---
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
failSwapOn: false
```

En doe ```sudo kubeadm init --config config.yaml```, daarna geeft het dit bericht:

```
Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join <IP van machine>:6443 --token <token.token> \
	--discovery-token-ca-cert-hash sha256:<sha256 hash>
```

Het bovenstaande verwacht dat je k8s kan installeren op de pods. Dat is bij de EJBCA docker containers niet (makkelijk) mogelijk. 

Aan de hand van [deze vergelijking](https://github.com/kubernetes-sigs/kubespray/blob/master/docs/comparisons.md) is het waarschijnlijk makkelijker om Kubespray te gebruiken, zie daarvoor [deze tutorial](https://kubernetes.io/docs/setup/production-environment/tools/kubespray/). Dat gebruikt o.a. ansible playbooks om het installeren. Dus dat maar proberen.

Als je de inventory file wil maken, eerst de [Github repo](https://github.com/kubernetes-sigs/kubespray/tree/master) clonen. In de geclone repo gaan met ```cd``` en aan de hand van [deze stappen](https://github.com/kubernetes-sigs/kubespray/blob/master/docs/getting-started.md) de inventory maken. Hiervoor doe je de volgende basiscommando's:

```
cp -r inventory/sample inventory/mycluster
declare -a IPS=(10.10.1.3 10.10.1.4 10.10.1.5)
CONFIG_FILE=inventory/mycluster/hosts.yml python3 contrib/inventory_builder/inventory.py ${IPS[@]}
```

Als de inventory gemaakt is en je gekeken hebt of inventory/mycluster/group_vars/all/all.yml en inventory/mycluster/group_vars/k8s_cluster/k8s-cluster.yml juist zijn doe je dit commando, hiervoor moet sshd op poort 22 staan en moet je private-key geen wachtwoord hebben/in ssh-agent staan:

```
ansible-playbook -i inventory/mycluster/hosts.yml cluster.yml -b -v -K --private-key=~/.ssh/private_key
```

Het vraagt hierbij om het sudo wachtwoord van de hosts. Dan doet het totdat het gaat pingen, maar dat werkt niet. Het verwacht dat je nodes en master op verschillende IP's staan, dat zou kunnen, maar dan moet je een extra VM maken. Toch niet zo handig.

Met minikube proberen, installeren met de rpm-package op de [get started pagina](https://minikube.sigs.k8s.io/docs/start/), dan starten met ```minikube start --driver=docker```. Het kan zijn dat er iets fout gaat met iptables/docker network, dan moet je de docker.service en/of docker.socket herstarten met ```systemctl restart```. Daarna testen met ```minikube kubectl -- get po -A```, je kan het makkelijker maken door ```alias kubectl="minikube kubectl -- "``` toe te voegen aan de bashrc/zshrc enz. Daarna kun je de dashboard aanzetten met ```minikube dashboard```.

Om bij de dashboard te komen kun je gebruik maken van [deze tutorial](https://adamtheautomator.com/kubernetes-dashboard/) met aanpassingen van [deze site](https://kubernetes.io/docs/reference/access-authn-authz/authentication/#service-account-tokens). Eerst met ```kubectl --namespace kubernetes-dashboard edit service kubernetes-dashboard``` "type: ClusterIP" veranderen in "type: Nodeport". Daarna de kubernetes-dashboard pod verwijderen zodat het opnieuw opstart. Daarna een serviceaccount maken met ```kubectl create serviceaccount dashboard -n kubernetes-dashboard``` en de bijbehorende token met ```kubectl create token dashboard -n kubernetes dashboard```. De token gebruik je om in te loggen bij de dashboard. Daarna een clusterrolebinding regel maken zodat je de rechten hebt: ```kubectl create clusterrolebinding dashboard-admin -n kubernetes-dashboard  --clusterrole=cluster-admin  --serviceaccount=default:dashboard```. Daarna kun je bij de dashboard komen door naar https://*IP van service*:*NodePort*/#/login te gaan.

Ik snap niet precies hoe het netwerk van minikube werkt. De poorten die je krijgt met ```kubectl get services -A``` of vergelijkbaar staan niet bij ```docker ps```. Misschien moet het daarvoor met "Ingress" gedaan worden, zie voor de verschillen tussen ClusterIP, NodePort en Ingress [deze site](https://www.howtogeek.com/devops/kubernetes-clusterip-nodeport-or-ingress-when-to-use-each/). Voor het port forwarden van het dashboard moet het waarschijnlijk van de "docker" zone naar de "public" of andere default zone gaan. Zie daarvoor [firewalld policy](https://firewalld.org/documentation/man-pages/firewalld.policy.html).

Het is sowieso beter kubernetes de containers/pods te laten genereren. Daarom moet je opnieuw EJBCA installeren aan de hand van [Deploy EJBCA container in MicroK8s](https://doc.primekey.com/ejbca/tutorials-and-guides/tutorial-deploy-ejbca-container-in-microk8s). Deze tutorial is gemaakt voor MicroK8s, het verschil is dat je gelijk commando's kan uitvoeren en niet hoeft te ssh-en naar de MicroK8s server. Dit betekent wel dat ook FreeIPA opnieuw ingesteld moet worden.

Bij kubeadm of kubespray installatie moet je om vanaf de laptop bij de web ui te komen de [dashboard plugin installeren](https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/) en waarschijnlijk een [certificate installeren](https://stackoverflow.com/a/45097914). Het heeft het over Jenkins, maar ik weet niet of dat nog actueel is. Zie ook [RBAC authorization](https://kubernetes.io/docs/reference/access-authn-authz/rbac/).
