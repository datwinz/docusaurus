---
sidebar_position: 6
title: Ansible
hide_title: true
---

## Met ansible vms en configuratie beheren

De [User Guide](https://docs.ansible.com/ansible/latest/user_guide/index.html) gebruiken voor een basis. Eerst een inventory maken aan de hand van [dit deel van de guide](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html). Afhankelijk wat je control node is localhost of de server die je beheert als node toevoegen. Daarnaast de andere vm's in een groep zetten.

Daarna een playbook maken met behulp van [dit deel](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_intro.html), hieronder staan collections die je kan gebruiken.

### Collections

* [ansible.builtin.yum_repository](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/yum_repository_module.html#ansible-collections-ansible-builtin-yum-repository-module)
* [ansible.builtin.dnf](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/dnf_module.html#dnf-module)
* [ansible.posix.firewalld](https://docs.ansible.com/ansible/latest/collections/ansible/posix/firewalld_module.html)
* [kubernetes.core.k8s](https://docs.ansible.com/ansible/2.9/modules/k8s_module.html#k8s-module)
* [kubernetes.core.k8s_service](https://docs.ansible.com/ansible/latest/collections/kubernetes/core/k8s_service_module.html#ansible-collections-kubernetes-core-k8s-service-module)
* [community.general.ipa_config](https://docs.ansible.com/ansible/latest/collections/community/general/ipa_config_module.html#ansible-collections-community-general-ipa-config-module)
* [community.general.ipa_group](https://docs.ansible.com/ansible/latest/collections/community/general/ipa_group_module.html#ansible-collections-community-general-ipa-group-module)
* [community.general.ipa_hbacrule](https://docs.ansible.com/ansible/latest/collections/community/general/ipa_hbacrule_module.html#ansible-collections-community-general-ipa-hbacrule-module)
* [community.general.ipa_host](https://docs.ansible.com/ansible/latest/collections/community/general/ipa_host_module.html#ansible-collections-community-general-ipa-host-module)
* [community.general.ipa_hostgroup](https://docs.ansible.com/ansible/latest/collections/community/general/ipa_hostgroup_module.html#ansible-collections-community-general-ipa-hostgroup-module)
* [community.general.ipa_role](https://docs.ansible.com/ansible/latest/collections/community/general/ipa_role_module.html#ansible-collections-community-general-ipa-role-module)
* [community.general.ipa_service](https://docs.ansible.com/ansible/latest/collections/community/general/ipa_service_module.html#ansible-collections-community-general-ipa-service-module)
* [community.general.ipa_user](https://docs.ansible.com/ansible/latest/collections/community/general/ipa_user_module.html#ansible-collections-community-general-ipa-user-module)
* [community.general.sefcontext](https://docs.ansible.com/ansible/latest/collections/community/general/sefcontext_module.html#ansible-collections-community-general-sefcontext-module)
* [community.libvirt.virt](https://docs.ansible.com/ansible/latest/collections/community/libvirt/virt_module.html#ansible-collections-community-libvirt-virt-module)
* [community.libvirt.virt_net](https://docs.ansible.com/ansible/latest/collections/community/libvirt/virt_net_module.html#ansible-collections-community-libvirt-virt-net-module)
* [community.libvirt.virt_pool](https://docs.ansible.com/ansible/latest/collections/community/libvirt/virt_pool_module.html#ansible-collections-community-libvirt-virt-pool-module)

[ansible.builtin](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/index.html#plugins-in-ansible-builtin) heeft nog veel meer plugins, bijvoorbeeld add_host, command, fail, git, group, package, pause, reboot enz. [ansible.posix](https://docs.ansible.com/ansible/latest/collections/ansible/posix/index.html#plugins-in-ansible-posix) heeft nog meer plugins voor mount, seboolean, selinux enz. [community.general](https://docs.ansible.com/ansible/latest/collections/community/general/index.html#plugins-in-community-general) heeft nog meer plugins voor bitbucket/github/gitlab, btrfs, freeipa, keycloak, selinex enz. Er zijn nog meer collections op [hier](https://docs.ansible.com/ansible/latest/collections/index.html) te vinden, bijvoorbeeld community.docker.

Voor het draaien van playbooks kun je een ansible system account maken met alleen de rechten die het nodig heeft. Dat doe je eerst met ```sudo useradd -r ansible```, daarna geef je met een sudoersbestand dat account de juiste rechten. Dit kan in het standaard sudoersbestand of in een apart bestand in de sudoers.d-map. Dit kan met ```sudo visudo -f /etc/sudoers.d/ansible```. Je moet /bin/sh ook als goedgestaan command toevoegen, omdat het op ssh-nodes ```'/bin/sh -c '"'"'sudo -H -S -n  -u root /bin/sh -c '"'"'"'"'"'"'"'"'echo BECOME-SUCCESS-ptjdikctsejxvnxqbindqlcxnexkwtpy``` doet. [Deze pagina](https://serverfault.com/a/1006924) geeft aan dat gebruikers in minder restictieve omgevingen ansible ALL privileges geven.

Je kan collections installeren met een [requirements file](https://docs.ansible.com/ansible/latest/collections_guide/collections_installing.html#install-multiple-collections-with-a-requirements-file), dan haalt die het automatisch op van ansible galaxy. ansible.builtin hoef je is logischerwijs al geinstalleerd, dus die hoef je niet in het requirements.yml-bestand te zetten. Je installeert het met ```ansible-galaxy install -r requirements.yml```.

Voor de basis-configuratie op alle drie hosts kun je een yaml-bestand maken voor de dnf/rpm packages en de firewall. Daarna kun je een yaml-bestand maken om de vms te starten. Voor de firewall moet je ook de firewall-library van python downloaden. Daar is de dnf-package "python3-firewall" voor, maar [met behulp van deze pagina](https://stackoverflow.com/a/72448139) ben ik er achter gekomen dat die is gemaakt voor python3.6. Dus je moet de package python36 downloaden ansible_python_interpreter als var in het yaml-bestand toevoegen. Dat moet ook bij het yaml-bestand om de vms te starten.

Om de syntax van playbooks te controleren kun je [ansible-lint](https://ansible.readthedocs.io/projects/lint/) gebruiken. Dat kun je installeren met pip(x), dnf (maar dan moet je wel een subscriber zijn van het "Red Hat Ansible Automation Platform") of als [GitHub Action](https://ansible.readthedocs.io/projects/lint/installing/#installing-ansible-lint-as-a-github-action). Dat is misschien ook om te bouwen naar een self-hosted GitLab pipeline. Voor een minder goede variant kun je ook ```ansible-playbook --syntax-check playbook.yml``` gebruiken.

Als je bij de packages playbook de "latest" state gebruikt, vind ansible-lint dat niet goed. Daarvoor moet je [dit toevoegen aan .ansible-lint](https://stackoverflow.com/a/61735133):

```
skip_list:
  - '403'
```

.ansible-lint zet je op dezelfde plek als de playbooks.

Om te testen kun je sudo privileges zonder wachtwoord instellen, maar daarna wil je voor geheimen ```ansible-vault``` gebruiken. Dit kan aan de hand van [deze beschrijving](https://docs.ansible.com/ansible/latest/tips_tricks/ansible_tips_tricks.html#tip-for-variables-and-vaults), maar dan per host een "host_vars" subdirectory. Als je dit doet kun je net zo goed alle variables die je in het "hosts"-bestand hebt gezet in de "vars" bestanden zetten. Encrypt de vault bestanden allemaal met hetzelfde wachtwoord. In het sudoers bestand nu NOPASSWORD weghalen en toestemming geven voor ALL commands. Dit aangezien ansible toestemming nodig heeft voor /bin/sh, wat in Rocky Linux 8 een link is naar bash, dus dat komt op hetzelfde neer als ALL. Hiernaar moet je playbooks draaien met ```ansible-playbook -i hosts --ask-vault-password playbook.yml``` en dat werkt het zoals eerst.

Een voorbeeld van de basisplaybooks, inventory enz. staat hieronder:

```
ansible/
├── .ansible-lint
├── firewall.yml
├── hosts
├── host_vars
│   ├── localhost
│   │   ├── vars
│   │   └── vault
│   ├── server
│   │   ├── vars
│   │   └── vault
│   └── workstation
│       ├── vars
│       └── vault
├── requirements.yml
├── start_vms.yml
└── verify-packages.yml
```
