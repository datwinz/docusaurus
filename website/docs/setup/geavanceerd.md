---
sidebar_position: 8
title: Geavanceerde zaken
hide_title: true
---

## Geavanceerde zaken

~~Met kubernetes de EJBCA-container opnieuw maken. Er is geen EJBCA-collection en de [playbooks van Keyfactor](https://github.com/Keyfactor/ansible-ejbca-signserver-playbooks) zijn om EJBCA in te richten in een container en niet om externe CA's te signen.~~ Kubernetes is te uitgebreid en complex hiervoor, dus ik ga Docker Swarm gebruiken. EJBCA instellen kan misschien met de [CLI van EJBCA](https://doc.primekey.com/ejbca/ejbca-operations/ejbca-operations-guide/command-line-interfaces#CommandLineInterfaces-Local_Command_line_interface) en de [command module](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/command_module.html#ansible-collections-ansible-builtin-command-module).

Daarna de vms opnieuw maken. Op host OS kun je aan de hand [deze webpagina](https://www.redhat.com/sysadmin/build-VM-fast-ansible) de vm's opzetten, dit is wat ingewikkelder. Je moet ook checken of selinux goed is ingesteld op de libvirt-map. ~~Dan kun je kubernetes instellen met de kubernetes.core collections, dat is waarschijnlijk ook ingewikkeld.~~

Op de server en workstation kun je waarschijnlijk met de community.general.ipa* de ipa instellen. Dit kan dan met een integrated DNS (named/bind).

Het idee is het om in deze volgorde te doen en om alles idempotent te automatiseren met ansible:

1. De vms maken. Deze op hetzelfde netwerk als de host. Ook een vm voor EJBCA.
2. De packages installeren en firewall instellen op de vms. Gebruik hiervoor dnf groups.
3. EJBCA instellen, IPA instellen, Taiga, gitlab en docusaurus instellen.
4. EJBCA, FreeIPA, Cockpit, Taiga, Gitlab en andere dashboards achter mijndomein.nl/* of *.mijndomein.nl/ zetten. Dit kan met een proxy zoals in de [tutorial van Taiga](https://community.taiga.io/t/taiga-30min-setup/170#configure-the-proxy-26).
5. Met Let's Encrypt of [EJBCA TLS server](https://doc.primekey.com/ejbca/tutorials-and-guides/tutorial-issue-tls-server-certificates-with-ejbca) en [client certificates](https://doc.primekey.com/ejbca/tutorials-and-guides/tutorial-issue-tls-client-certificates-with-ejbca) alles achter vertrouwde TLS zetten.
6. Wazuh, SAST, DAST en SCA instellen.
7. Autoupdates instellen, automatisch ansible draaien instellen, verdere automatisering instellen.
8. Docusaurus of een andere documentatie online zetten.
9. Erachter komen hoe certificaten voor applicaties werken. [SoftHSM](https://www.opendnssec.org/softhsm/) voor i.i.g. de root CA instellen.
10. Mail server: [Postfix SMTP opzetten](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/deploying_different_types_of_servers/assembly_mail-transport-agent_deploying-different-types-of-servers) en [Dovecot IMAP/POP3 opzetten](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/deploying_different_types_of_servers/configuring-and-maintaining-a-dovecot-imap-and-pop3-server_deploying-different-types-of-servers). Zijn volgens mij twee verschillende systemen, dus waarschijnlijk moet ik 1 van de twee kiezen.
