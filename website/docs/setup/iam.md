---
sidebar_position: 7
title: IAM
hide_title: true
---

## Met keycloak of zitadel IAM instellen

[Zitadel](https://zitadel.com/) verkoopt zich als een makkelijke manier, net zoals Auth0, maar dan open source. [https://www.keycloak.org/](https://www.keycloak.org/) verkoopt zich alleen als open source, daarom eerst maar Zitadel proberen. Ik weet niet meer zo goed waarom ik Boundary/Keycloak/Zitadel wilde installeren. Ik kan het gebruiken om alle adminconsoles achter 1 inlogmanier te zetten. Dit kan helpen met het verzekeren van principle of least privilege. Hiervoor moet ik alleen wel ook nog een beleid maken voor principle of least privilige.

De adminconsoles zijn ook nog niet definitief:

Het is teveel gedoe om telkens uit te zoeken hoe dat ene programma zijn ports moet forwarden. Dus een wireguard vpn installeren aan de hand van [deze guide](https://www.digitalocean.com/community/tutorials/how-to-set-up-wireguard-on-rocky-linux-8). Dit doe je door eerst een key pair te generen met ```wg genkey``` en ```wg pubkey```. Daarna de private key aan /etc/default/wg0.conf toe te voegen met de ListenPort, de Adress en SaveConfig op true. Hierna voeg je de poort toe aan firewalld in de public zone en zet voeg je het wg0 interface aan de internal zone toe. Hierna kun je de met systemctl de wg-quick@wg0.service enablen en starten. Hierna de instellingen op de client toevoegen, mac zoals hieronder:

```
[Interface]
 34 PrivateKey = <mac private key>
 35 Address = 10.8.0.2/24
 36 
 37 [Peer]
 38 PublicKey = gPCC3CgiYMANnT07kN0qVNqf3rzQEOpI7qIouXrVaiU=
 39 AllowedIPs = 10.8.0.1/24
 40 Endpoint = <public router ip>:51820
```

Hiervoor moet je ook op je router de port 51820 vrijgeven.

Ik heb nu de VM's in hetzelfde netwerk als de host OS gezet, dus wireguard is niet meer nodig. Ikheb het dus verwijderd.

* ~~Voor de k8s-console moet een [firewalld policy](https://firewalld.org/documentation/man-pages/firewalld.policy.html) worden ingesteld.~~
    * ~~Hoeft niet met firewalld policies, het lukte me uberhaupt niet om het met firewalld-policies werkend te krijgen. Aan de hand van [deze link](https://stackoverflow.com/a/53830578) kun je het openzetten met ```kubectl proxy --address='0.0.0.0' --disable-filter=true```. Dit werkt zo: als je dashboard geinstalleerd hebt, start de pod automatisch als minikube opstart. Dan hoef je niet de proxy  te starten met ```minikube dashboard --url --port=8001```, maar kan het ook handmatig met ```kubectl proxy```. Dit open automatisch op poort 8001, dus hiervoor moet wel 8001/tcp goedgekeurd worden in de host OS firewall.~~
* In plaats van k8s, Docker Swarm gebruiken aan de hand van [deze guide](https://docs.docker.com/engine/swarm/stack-deploy/)
* Voor EJBCA moet het opnieuw geinstalleerd worden met ansible en de [CLI](https://doc.primekey.com/ejbca/ejbca-operations/ejbca-operations-guide/command-line-interfaces), daarnaast moet weer een firewalld policy ingesteld worden.
* Voor FreeIPA moet het opnieuw geinstalleerd worden met ansible en moet opnieuw een firewalld policy ingesteld worden. FreeIPA moet geinstalleerd worden met een integrated DNS (named/bind).

Oftewel de "Geavanceerde zaken" moeten eerst gedaan worden.
