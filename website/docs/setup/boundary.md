---
sidebar_position: 4
title: Boundary
hide_title: true
---

## Boundary instellen voor key management en ssh

Aan de hand van [self-managed Boundary Quick Start](https://developer.hashicorp.com/boundary/tutorials/oss-getting-started).

Doen op eigen laptop of pc (i.i.g. om te testen), dan kun je makkelijk de admin web UI gebruiken. ```boundary dev``` doen in een tmux- of screensessie want het draait in de voorgrond.

Werkt niet zoals ik wil, want de hosts moeten zover ik kan zien allemaal in hetzelfde netwerk zitten. Misschien zoeken naar iets met agents die je installeert op de hosts. Keyword: identity access management/IAM. Misschien met [Keycloak](https://www.keycloak.org/) of [Zitadel](https://zitadel.com/), gaat o.a. om "machine to machine communication".
