---
sidebar_position: 1
title: Libvirt
hide_title: true
---

## Libvirt

### Vagrant Libvirt plugin

Om de libvirt-plugin te installeren op het systeem eerst ```export CONFIGURE_ARGS="with-libvirt-include=/usr/include/libvirt with-libvirt-lib=/usr/lib64"``` doen en dan ```vagrant plugin install vagrant-libvirt```.

Daarnaast moet je de libssh en krb5-libs libraries opnieuw bouwen voor vagrant. Dat doe je met:

```
sudo su -

mkdir -p ~/vagrant-build && cd ~/vagrant-build

dnf download --source krb5-libs libssh

rpm2cpio krb5-*.src.rpm | cpio -idmv krb5-*.tar.gz
tar xzf krb5-*.tar.gz
pushd krb5-*/src
./configure
make
sudo cp -a lib/crypto/libk5crypto.so.3* /opt/vagrant/embedded/lib64/
popd

rpm2cpio libssh-*.src.rpm | cpio -imdv  libssh-*.tar.xz
tar xJf libssh-*.tar.xz
mkdir build
pushd build
cmake ../libssh-*  -DOPENSSL_ROOT_DIR=/opt/vagrant/embedded
make
sudo cp lib/libssh* /opt/vagrant/embedded/lib64/
popd
```

En dan werkt het waarschijnlijk nog steeds niet. Dus vagrant verwijderen en Docker of podman doen: [vagrant-libvirt docker/podman guide](https://vagrant-libvirt.github.io/vagrant-libvirt/installation.html#docker--podman).

Je doet eerst ```podman pull vagrantlibvirt/vagrant-libvirt:latest```. En dan voeg je onderstaande toe aan je .bashrc:

```
vagrant(){
  podman run -it --rm \
    -e LIBVIRT_DEFAULT_URI \
    -v /var/run/libvirt/:/var/run/libvirt/ \
    -v ~/.vagrant.d:/.vagrant.d \
    -v $(realpath "${PWD}"):${PWD} \
    -w "${PWD}" \
    --network host \
    --entrypoint /bin/bash \
    --security-opt label=disable \
    docker.io/vagrantlibvirt/vagrant-libvirt:latest \
      vagrant $@
}
```

Hier mee roep je niet meer het programma aan als je ```vagrant``` typt, maar de container. Het nadeel is dat het automatisch alleen een vnc-beeldverbinding maakt en geen spice (wat volgens mij nodig is voor virt-manager op mac). Daarnaast blijven sommige boxes vastzitten op "Waiting for domain to get an IP address...". ~~En voor ipa-server/FreeIPA moet één van de vms de DHCP server zijn, wat waarschijnlijk niet werkt met Vagrant.~~

### Handmatige libvirt VM

Als je dat bij installeren van de OS niet gedaan hebt, eerst een extra partitie maken voor /var/lib/libvirt. De standaardlocatie is namelijke onder root en die partitie is waarschijnlijk niet zo groot. Als je aparte partitie hebt gemaakt voor libvirt, moet je de selinux context restoren in de /var/lib/libvirt dir. Anders gaat het zeuren dat het de bestanden voor het virtuele netwerk niet kan lezen. Dat doe je met ```sudo restore -r /var/lib/libvirt```.

ISO downloaden en installeren met onderstaande commando's:

```bash
cd /var/lib/libvirt/boot/
sudo wget https://download.rockylinux.org/pub/rocky/8/isos/x86_64/Rocky-8.7-x86_64-dvd1.iso
export ISO="/var/lib/libvirt/boot/Rocky-8.7-x86_64-dvd1.iso" # Installation media
export NET="virbr0" # bridge name
export OS="rocky8" # os type
export VM_IMG="/var/lib/libvirt/images/rocky8.qcow2" # VM image on disk
sudo virt-install \
--virt-type=kvm \
--name centos8 \
--ram 1536 \
--vcpus=1 \
--os-variant=${OS} \
--virt-type=kvm \
--hvm \
--cdrom=${ISO} \
--network=bridge=${NET},model=virtio \
--graphics vnc \
--disk path=${VM_IMG},size=50,bus=virtio,format=qcow2,sparse=yes
```

Dan moet je met VNC of met virt-manager over ssh het installatieproces doorlopen. Of een anaconda/kickstart install, maar dat is moeilijk, zie daarvoor: [RHEL 8 advanced installation](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/performing_an_advanced_rhel_8_installation/index).

Je kan over ssh met virt-manager de vm managen en ook installeren, zie [ubuntu virtualization tools](https://ubuntu.com/server/docs/virtualization-virt-tools). Het installeren kan denk ik ook met virt-viewer i.p.v. virt-manager. Hiervoor moet je virt-manager installeren op je server waar je de vms installeert en op de pc/laptop waarvan af je je server managet. Je kan op de pc/laptop ook deze [docker container](https://hub.docker.com/r/mber5/virt-manager) gebruiken, eerst pullen, dan een docker-compose.yml maken met de juiste instellingen aan de hand van de template op dockerhub. Dan in de map met de yaml-file ```docker-compose up -d``` doen en naar [localhost:8185](localhost:8185) gaan.

Dit gaat over ssh, daarvoor moet je op de server het account waarmee je over ssh inlogt in de libvirt group zetten met ```sudo usermod -a -G libvirt gebruiker``` ([bron](https://access.redhat.com/solutions/5185571)). Daarna ga je op de pc/laptop naar de map waar je ssh-keys in staan en doe je met jouw gegevens ([bron](https://fabianlee.org/2019/02/16/kvm-virt-manager-to-connect-to-a-remote-console-using-qemussh/)):

```
virt-manager -c 'qemu+ssh://gebruiker@192.168.1.121/system?keyfile=id_rsa'
```

Je kan denk ik geen ssh-key passphrase intypen, dus je moet ssh-agent draaien of een key zonder passphrase hebben. Als ssh-agent draait, kan het ook automatisch de keyfile herkennen en dus automatisch inloggen.

vm1 is een server en vm2 is een workstation.

~~Op server met nmtui een manual/handmatig IP-adres instellen binnen het bereik dat je ziet als je op de host OS ```sudo virsh net-dumpxml default``` doet (of in plaats van default het netwerk dat je gebruikt voor je vms). Als gateway het IP-adres instellen wat je bij default ziet staan als je op server ```ip route``` doet. Als DNS Server het IP-adres invullen wat op server onder /etc/resolv.conf staat. Daarna server herstarten, checken of de vm het ingestelde IP-adres heeft en check of je nog verbonden bent met het internet.~~

~~Dan op de host OS met ```sudo virsh net-edit default``` de rijen van ```<dhcp>``` t/m ```</dhcp>``` verwijderen.~~

Dit hoeft niet, voor FreeIPA hoeft het geen DHCP-server te zijn, alleen een DNS-server. En dat wordt zelfs alleen maar aangeraden, is niet verplicht.

### Virsh

* ["ssh" naar virsh vm](https://askubuntu.com/questions/576437/virsh-ssh-into-a-guest-vm)
* [libvirt port-forwarding van wiki](https://wiki.libvirt.org/page/Networking#Forwarding_Incoming_Connections)
* [libvirt port-forwarding van stackexchange](https://unix.stackexchange.com/questions/350339/how-to-port-forward-ssh-in-virt-manager)
