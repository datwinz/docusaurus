---
sidebar_position: 2
title: EJBCA Docker
hide_title: true
---

## ejbca-ce docker

Docker containers maken aan de hand van "[Start out with EJBCA Docker container](https://doc.primekey.com/ejbca/tutorials-and-guides/tutorial-start-out-with-ejbca-docker-container)", maar dan met ```podman``` en ```podman-compose```. Voor de docker-compose.yml dit gebruiken:

```
version: '3'
networks:
  access-bridge:
    driver: bridge
  application-bridge:
    driver: bridge
services:
  ejbca-database:
    container_name: ejbca-database
    image: "library/mariadb:latest"
    networks:
      - application-bridge
    environment:
      - MYSQL_ROOT_PASSWORD=Verysecurepassword123
      - MYSQL_DATABASE=ejbca
      - MYSQL_USER=ejbca
      - MYSQL_PASSWORD=Verysecurepassword456
    volumes:
      - ./datadbdir:/var/lib/mysql:rw
    restart: unless-stopped
  ejbca-node1:
    hostname: ejbca-node1
    container_name: ejbca
    image: keyfactor/ejbca-ce:latest
    depends_on:
      - ejbca-database
    networks:
      - access-bridge
      - application-bridge
    environment:
      - DATABASE_JDBC_URL=jdbc:mariadb://ejbca-database:3306/ejbca?characterEncoding=UTF-8
      - LOG_LEVEL_APP=INFO
      - LOG_LEVEL_SERVER=INFO
      - TLS_SETUP_ENABLED=simple
      - DATABASE_PASSWORD=Verysecurepassword456
    ports:
      - "80:8080"
      - "443:8443"
    restart: unless-stopped
```

Als je de tekst op de site gebruikt zit er een indentatiefout op regel 22 (```container_name: ejbca```), dus die kun je het beste verwijderen en opnieuw typen. Je moet de containers maken als root, want een normale gebruiker kan in podman geen webserver aanzetten. De mariadb server geeft exit code 1, /var/lib/mysql in die container heeft namelijk niet de juiste permissies. De mariadb container werkt niet goed met podman (of i.i.g. niet om de juiste permissies in te stellen voor /var/lib/mysql). Dat zegt deze [bron](https://stackoverflow.com/questions/70675139/how-do-i-access-a-db-container-when-using-podman-compose) ook.

Dus centos docker repo toevoegen en docker-ce downloaden. Je kan je gebruiker aan de docker groep toevoegen zodat je niet ```sudo``` hoeft te gebruiken, werkt waarschijnlijk pas na een reboot. Hierbij hetzelfde compose-bestand gebruiken.

~~Checken met ```docker ps``` naar welke poort de web interface gaat. Dan met firewall-cmd een port-forwarding regel instellen ([bron](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/sec-port_forwarding)), bijvoorbeeld zo:~~

~~sudo firewall-cmd --add-forward-port=port=8443:proto=tcp:toport=443~~

~~sudo firewall-cmd --add-masquerade~~

Dit hoeft niet vanwege een [bug tussen docker en firewalld](https://github.com/firewalld/firewalld/issues/869#issuecomment-1246740576). De eerste poort bij ```docker * -p <poort>:<poort> *``` of vergelijkbaar in het compose-bestand wordt hierdoor opengezet naar het host netwerk.

Check of je vanaf je client bij de EJBCA Administration kan.

De mariadb container ingaan met ```docker exec -it ejbca-database /bin/bash``` en /usr/bin/mariadb-secure-installation uitvoeren. De stappen doorlopen en de containers herstarten.

Verbinden op [https://192.168.1.121/ejbca/adminweb/](https://192.168.1.121/ejbca/adminweb/) of soortgelijk en de stappen doorlopen. Het SuperAdmin-certificaat opslaan op een veilige plek, bijvoorbeeld een versleutelde schijf(kopie). Daarna door met "[Create your first Root CA using EJBCA](https://doc.primekey.com/ejbca/tutorials-and-guides/tutorial-create-your-first-root-ca-using-ejbca)".
